"use strict";
const hamb = document.querySelector(".hamb__field");
const popup = document.querySelector(".popup");
const menu = document.querySelector(".hamb-menu").cloneNode(true);

hamb.addEventListener("click", humbHendler);

function humbHendler(e) {
  e.preventDefault();
  popup.classList.toggle("open");
  changePopup();
  hamb.classList.toggle("active");
}
function changePopup() {
  popup.appendChild(menu);
}

document.addEventListener("click", function (event) {
  if (!hamb.contains(event.target) && !popup.contains(event.target)) {
    popup.classList.remove("open");
    hamb.classList.remove("active");
  }
});
