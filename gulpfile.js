const gulp = require("gulp");
const sass = require("gulp-sass")(require("sass"));
const rename = require("gulp-rename");
const browserSync = require("browser-sync").create();
const tinypng = require("gulp-tinypng");
const autoprefixer = require("gulp-autoprefixer");
const minifyjs = require("gulp-js-minify");
const concatCss = require("gulp-concat-css");
const cleanCSS = require("gulp-clean-css");
const del = require("del");

gulp.task("buildStyle", () => {
  return gulp
    .src("./src/styles_scss/*.scss")
    .pipe(sass().on("error", sass.logError))
    .pipe(gulp.dest("./src/styles_css"));
});
gulp.task("tinypng", () => {
  return gulp
    .src("./src/images/*")
    .pipe(tinypng("Tf3xFMPYlz0r2SsdsXwQtghJcLspQYym"))
    .pipe(gulp.dest("dist/img.min"));
});
gulp.task("minify-js", function () {
  return gulp
    .src("./src/scripts/index.js")
    .pipe(minifyjs())
    .pipe(rename("script.min.js"))
    .pipe(gulp.dest("dist/script.min"));
});
gulp.task("concatCss", function () {
  return gulp
    .src("./src/styles_css/*.css")
    .pipe(concatCss("./style_concat.css"))
    .pipe(gulp.dest("./src/styles_css/style_concat"));
});
gulp.task("prefixer", () => {
  return gulp
    .src("./src/styles_css/style_concat/style_concat.css")
    .pipe(
      autoprefixer({
        overrideBrowserList: ["last 2 versions"],
        cascade: false,
      })
    )
    .pipe(rename("style_prefix.css"))
    .pipe(gulp.dest("./src/styles_css/style_prefix/"));
});
gulp.task("clean-css", () => {
  return gulp
    .src("./src/styles_css/style_prefix/style_prefix.css")
    .pipe(cleanCSS({ compatibility: "ie8" }))
    .pipe(rename("style_min.css"))
    .pipe(gulp.dest("./dist/style.min"));
});

gulp.task("taskDel", (cb) => {
  return del("dist/*");
});

gulp.task(
  "build",
  gulp.series([
    "taskDel",
    "buildStyle",
    "tinypng",
    "minify-js",
    "concatCss",
    "prefixer",
    "clean-css",
  ])
);

gulp.task("dev", () => {
  browserSync.init({
    server: "./",
  });
  gulp.watch(
    "./src/styles_scss/**.scss",
    gulp.series("buildStyle", "prefixer", "concatCss", "clean-css", "minify-js")
  );
  gulp.watch("./src/js/index.js", gulp.series("minify-js"));
  gulp.watch("./index.html").on("change", browserSync.reload);
  gulp.watch("./dist/style.min/style_min.css").on("change", browserSync.reload);
  gulp
    .watch("./dist/script.min/script.min.js")
    .on("change", browserSync.reload);
});
